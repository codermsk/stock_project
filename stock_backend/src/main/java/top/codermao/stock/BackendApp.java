package top.codermao.stock;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author msk
 * @date 2024/4/9 22:12
 * @Description
 */
@SpringBootApplication
//扫描持久层mapper接口，生成代理对象并维护到spring的IOC容器中
@MapperScan("top.codermao.stock.mapper")
public class BackendApp {
    public static void main(String[] args) {
        SpringApplication.run(BackendApp.class, args);
    }
}
