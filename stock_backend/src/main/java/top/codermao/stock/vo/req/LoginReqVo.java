package top.codermao.stock.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author msk
 * @date 2024/4/10 10:17
 * @Description
 */
@ApiModel("登录请求数据封装")
@Data
public class LoginReqVo {
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;
    /**
     * 密码
     */
    @ApiModelProperty("明文密码")
    private String password;
    /**
     * 验证码
     */
    @ApiModelProperty("验证码")
    private String code;
    /**
     * redis
     */
    @ApiModelProperty("redis的sessionId")
    private String sessionId;
}