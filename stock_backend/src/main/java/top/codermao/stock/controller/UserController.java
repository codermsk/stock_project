package top.codermao.stock.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.codermao.stock.pojo.entity.SysUser;
import top.codermao.stock.service.UserService;
import top.codermao.stock.vo.req.LoginReqVo;
import top.codermao.stock.vo.resp.LoginRespVo;
import top.codermao.stock.vo.resp.R;

import java.util.Map;

/**
 * @author msk
 * @date 2024/4/9 22:32
 * @Description
 */
@Api(tags = "用户相关接口处理器")
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 根据用户名查询用户信息
     *
     * @param userName
     * @return
     */
    @ApiOperation(value = "根据用户名查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "用户名", dataType = "string", required = true, type = "path")
    })
    @GetMapping("/user/{userName}")
    public SysUser getUserByUserName(@PathVariable("userName") String userName) {
        return userService.getUserByUserName(userName);
    }

    /**
     * 用户登录功能
     * @param vo
     * @return
     */
    @ApiOperation(value = "用户登录功能")
    @PostMapping("/login")
    public R<LoginRespVo> login(@RequestBody LoginReqVo vo) {
        return userService.login(vo);
    }

    /**
     * 生成图片验证码功能
     *
     * @return
     */
    @ApiOperation("验证码生成")
    @GetMapping("/captcha")
    public R<Map> getCaptchaCode() {
        return userService.getCaptchaCode();
    }

}
