package top.codermao.stock.service;

import top.codermao.stock.pojo.entity.SysUser;
import top.codermao.stock.vo.req.LoginReqVo;
import top.codermao.stock.vo.resp.LoginRespVo;
import top.codermao.stock.vo.resp.R;

import java.util.Map;

/**
 * @author msk
 * @date 2024/4/9 22:16
 * @Description 定义用户服务接口
 */
public interface UserService {
    /**
     * 根据用户查询用户信息
     * @param userName 用户名称
     * @return
     */
    SysUser getUserByUserName(String userName);

    /**
     * 登录方法
     * @param vo
     * @return
     */
    R<LoginRespVo> login(LoginReqVo vo);

    /**
     * 生成图片验证码功能
     * @return
     */
    R<Map> getCaptchaCode();
}
