package top.codermao.stock.service.impl;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import top.codermao.stock.constant.StockConstant;
import top.codermao.stock.pojo.entity.SysUser;
import top.codermao.stock.service.UserService;
import top.codermao.stock.utils.IdWorker;
import top.codermao.stock.vo.req.LoginReqVo;
import top.codermao.stock.vo.resp.LoginRespVo;
import top.codermao.stock.vo.resp.R;
import top.codermao.stock.vo.resp.ResponseCode;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author msk
 * @date 2024/4/9 22:25
 * @Description
 */
@Service("userService")
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private top.codermao.stock.mapper.SysUserMapper sysUserMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 根据用户名称查询用户信息
     *
     * @param userName 用户名称
     * @return
     */
    @Override
    public SysUser getUserByUserName(String userName) {
        SysUser user = sysUserMapper.findByUserName(userName);
        return user;
    }

    /**
     * 用户登录功能
     *
     * @param vo
     * @return
     */
    @Override
    public R<LoginRespVo> login(LoginReqVo vo) {
        //1.判断参数是否合法
        if (vo == null || StringUtils.isBlank(vo.getUsername()) || StringUtils.isBlank(vo.getPassword())) {
            return R.error(ResponseCode.DATA_ERROR);
        }

        //验证输入的验证码是否存在
        if (StringUtils.isBlank(vo.getCode()) || StringUtils.isBlank(vo.getSessionId())) {
            return R.error(ResponseCode.CHECK_CODE_ERROR);
        }
        //判断redis中保存的验证码与输入的验证码是否相同（比较时忽略大小写）
        String redisCode = (String) redisTemplate.opsForValue().get(StockConstant.CHECK_PREFIX + vo.getSessionId());
        if (StringUtils.isBlank(redisCode)) {
            return R.error(ResponseCode.CHECK_CODE_TIMEOUT);
        }
        if (!redisCode.equalsIgnoreCase(vo.getCode())) {
            return R.error(ResponseCode.CHECK_CODE_ERROR);
        }

        //2.根据用户名去数据库查询用户信息，获取密码的密文
        SysUser dbUser = sysUserMapper.findByUserName(vo.getUsername());
        if (dbUser == null) {
            //用户不存在
            return R.error(ResponseCode.ACCOUNT_NOT_EXISTS);
        }
        //3.调用密码匹配器匹配输入的明文密码和数据库的密文密码
        if (!passwordEncoder.matches(vo.getPassword(), dbUser.getPassword())) {
            return R.error(ResponseCode.USERNAME_OR_PASSWORD_ERROR);
        }
        //4.响应
        LoginRespVo respVo = new LoginRespVo();
//        respVo.setId(dbUser.getId());
//        respVo.setUsername(dbUser.getUsername());
//        respVo.setNickName(dbUser.getNickName());
//        respVo.setPhone(dbUser.getPhone());
        //发现loginRespVo与SysUser对象属性名称和类型一致
        //必须保证属性名称和类型一致，否则不能用
        BeanUtils.copyProperties(dbUser, respVo);
        return R.ok(respVo);
    }

    /**
     * 生成图片验证码功能
     *
     * @return
     */
    @Override
    public R<Map> getCaptchaCode() {
         /*
            参数1：图片高度
            参数2：图片宽度
            参数3：图片中包含验证码的长度
            参数4：干扰线数量
         */
        LineCaptcha captcha = CaptchaUtil.createLineCaptcha(250, 40, 4, 5);
        //设置背景颜色
        captcha.setBackground(Color.LIGHT_GRAY);
        //自定义生成验证码的规则
        /* captcha.setGenerator(new CodeGenerator() {
            @Override
            public String generate() {
                //自定义校验码生成逻辑
                return null;
            }

            @Override
            public boolean verify(String s, String s1) {
                //匹配逻辑
                return false;
            }
        });*/
        //获取校验码
        String checkCode = captcha.getCode();
        //获取经过base64编码处理的图片数据
        String imageData = captcha.getImageBase64();
        //2.生成会话的sessionId,转化成string，避免精度丢失
        String sessionId = String.valueOf(idWorker.nextId());
        log.info("当前生成的图片校验码:{},会话id:{}", checkCode, sessionId);
        //3.后台将sessionId作为key，校验码作为value保存到redis中(使用redis模拟session的行为，通过过期时间的设置)
        //给key加上业务前缀
        redisTemplate.opsForValue().set(StockConstant.CHECK_PREFIX + sessionId, checkCode, 1, TimeUnit.MINUTES);
        //4.组装数据
        HashMap<String, String> data = new HashMap<>();
        data.put("imageData", imageData);
        data.put("sessionId", sessionId);
        //5.响应数据
        return R.ok(data);
    }
}
