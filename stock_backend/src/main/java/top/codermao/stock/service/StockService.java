package top.codermao.stock.service;

import top.codermao.stock.pojo.domain.*;
import top.codermao.stock.vo.resp.PageResult;
import top.codermao.stock.vo.resp.R;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author msk
 * @date 2024/4/17 20:51
 * @Description 股票服务接口
 */
public interface StockService {
    /**
     * 获取国内大盘最新的数据
     * @return
     */
    R<List<InnerMarketDomain>> getInnerMarketInfo();

    /**
     *需求说明: 获取沪深两市板块最新数据，以交易总金额降序查询，取前10条数据
     * @return
     */
    R<List<StockBlockDomain>> sectorAllLimit();

    /**
     * 分页查询最新的股票交易数据
     * @param page 当前页
     * @param pageSize 每页大小
     * @return
     */
    R<PageResult<StockUpdownDomain>> getStockInfoByPage(Integer page, Integer pageSize);

    /**
     * 查询部分最新的股票交易数据
     *
     * @return
     */
    R<List<StockUpdownDomain>> getStockInfoSegment();

    /**
     * 统计最新股票交易日内每分钟的涨跌停的股票数量
     * @return
     */
    R<Map<String, List>> getStockUpDownCount();

    /**
     * 导出指定页码的股票信息
     * @param page  当前页
     * @param pageSize 每页大小
     * @param response
     */
    void exportStockUpDownInfo(Integer page, Integer pageSize, HttpServletResponse response);

    /**
     * 统计大盘T日和T-1日每分钟交易量的统计
     * @return
     */
    R<Map<String, List>> getComparedStockTradeAmt();

    R<Map> getIncreaseRangeInfo();

    /**
     * 获取指定股票T日分时数据
     * @param stockCode
     * @return
     */
    R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(String stockCode);

    /**
     * 查询指定股票的日k线数据
     * @param stockCode 股票编码
     * @return
     */
    R<List<Stock4EvrDayDomain>> getStockScreenDkLine(String stockCode);
}
