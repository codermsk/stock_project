package top.codermao.stock;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.codermao.stock.mapper.SysUserMapper;
import top.codermao.stock.pojo.entity.SysUser;

import java.util.List;

/**
 * @author msk
 * @date 2024/4/19 17:25
 * @Description
 */
@SpringBootTest
public class TestPageHelper {
    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 测试分页
     */
    @Test
    public void test01() {
        Integer page = 2; //当前页
        Integer pageSize = 5; //每页大小
        PageHelper.startPage(page, pageSize);
        List<SysUser> all = sysUserMapper.findAll();
        //将查询到的page封装到PageInfo下就可以获取分页的各种数据
        PageInfo<SysUser> pageInfo = new PageInfo<>(all);
        int pageNum = pageInfo.getPageNum();//获取当前页
        int pages = pageInfo.getPages(); //总页数
        int pageSize1 = pageInfo.getPageSize();//每页大小
        int size = pageInfo.getPageSize();//当前页的记录数
        long total = pageInfo.getTotal();//总记录数
        List<SysUser> list = pageInfo.getList();//当前页的具体内容
        System.err.println("list" + list);
    }
}
