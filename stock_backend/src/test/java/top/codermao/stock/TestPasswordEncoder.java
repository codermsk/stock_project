package top.codermao.stock;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author msk
 * @date 2024/4/10 14:33
 * @Description
 */
@SpringBootTest
public class TestPasswordEncoder {
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 测试加密
     */
    @Test
    public void test() {
        String password = "123456";
        String encode = passwordEncoder.encode(password);
//        $2a$10$M1HQKTN9qIijFPo2Sd50guVRe/qfI5.a6Q5/n0pFRZVAObWoBw3FW
        System.out.println(encode);
    }

    @Test
    public void test02() {
        String pwd = "123456";
        String enPwd = "$2a$10$M1HQKTN9qIijFPo2Sd50guVRe/qfI5.a6Q5/n0pFRZVAObWoBw3FW";
        boolean isSuccess = passwordEncoder.matches(pwd, enPwd);
        System.out.println(isSuccess ? "密码匹配成功" : "密码匹配失败");
    }
}
