package top.codermao.stock;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author msk
 * @date 2024/5/4 15:26
 * @Description
 */
@SpringBootApplication
//扫描持久层mapper接口，生成代理对象并维护到spring的IOC容器中
@MapperScan("top.codermao.stock.mapper")
public class JobApp {
    public static void main(String[] args) {
        SpringApplication.run(JobApp.class, args);
    }
}
