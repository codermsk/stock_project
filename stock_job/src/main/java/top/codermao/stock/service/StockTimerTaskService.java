package top.codermao.stock.service;

/**
 * @author msk
 * @date 2024/5/4 21:14
 * @Description 定义采集股票数据的定时任务的服务接口
 */
public interface StockTimerTaskService {
    /**
     * 获取国内大盘的实时数据信息
     */
    void getInnerMarketInfo();
}