package top.codermao.stock.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author msk
 * @date 2024/5/4 15:31
 * @Description 定义http客户端工具bean
 */
@Configuration
public class HttpClientConfig {
    /**
     * 定义http客户端bean
     *    RestTemplate是一个java Http的客户端，可以模拟浏览器的访问行为，获取接口数据；
     * @return
     */
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
