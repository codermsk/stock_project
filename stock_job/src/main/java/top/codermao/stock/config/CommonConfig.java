package top.codermao.stock.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.codermao.stock.utils.IdWorker;

/**
 * @author msk
 * @date 2024/4/10 14:30
 * @Description
 */
@Configuration
//@EnableConfigurationProperties({StockInfoConfig.class}) //开启对象相关配置对象的加载
public class CommonConfig {
    /**
     * 基于雪花算法保证生成的id唯一
     * @return
     */
    @Bean
    public IdWorker idWorker(){
        /*
         * 参数1：机器id
         * 参数2：机房id
         * 机房和机器编号一般由运维人员进行唯一性规划
         */
        return new IdWorker(1l,2l);
    }
}
