# 业务需求：查询最新的国内A股大盘数据（上证 深证）
# 1、最新是指最新股票交易时间点下对应的数据 2、A股：上证 深证
# 3、相关的表：stock_market_index_info 4、确定需要查询的字段 大盘代码、大盘名称、开盘点、最新点、前收盘点、交易量、交易金额、涨跌值、涨幅、振幅、当前日期
select
    market_code AS code,
    market_name AS name,
    open_point AS openPoint,
    cur_point AS curPoint,
    pre_close_point AS preClosePoint,
    trade_amount AS tradeAmt,
    trade_volume AS tradeVol,
    cur_point-pre_close_point AS upDown,
    (cur_point-pre_close_point)/pre_close_point AS rose,
    (max_point-min_point)/pre_close_point AS amplitude,
    cur_time AS curTime
from stock_market_index_info
where cur_time = '2022-07-07 14:52:00'
  and market_code in ('sh000001', 'sz399001');
# 思考：1、一个大盘比如上证，在某个时间点最多有1条数据 2、大盘数据日积月累数据量很大，查询最好走索引
# 所以，为stock_market_index_info添加唯一索引：联合唯一索引
# 联合主键索引 联合唯一索引 联合普通索引

# 业务需求：分页查询最新股票交易数据，根据涨幅降序排序
# 好处：走索引，避免全表排序查询，速度更快
select
    sri.trade_amount as tradeAmt,
    sri.pre_close_price as preClosePrice,
    (sri.max_price- sri.min_price)/sri.pre_close_price as amplitude,
    sri.stock_code as code,
    sri.stock_name as name,
    sri.cur_time   as curDate,
    sri.trade_volume as tradeVol,
    (sri.cur_price-sri.pre_close_price) as increase,
    (sri.cur_price- sri.pre_close_price)/sri.pre_close_price as upDown,
    sri.cur_price as tradePrice
from stock_rt_info as sri
where sri.cur_time='2022-07-07 14:55:00'
order by upDown desc
# 业务需求：统计沪深两市T日（当前股票交易日）每分钟达到涨跌停股票的数据
# 如果不在股票的交易日内，则统计最近的股票交易日下的数据
# 分析业务：
# 1.相关的表。
# 2.条件以查询涨停为例，1.统计指定日期范围内的股票数据，从开盘到最新股票交易时间点。
# 											2.涨停与涨幅有关，涨幅与股票的当前价-前收盘价/前收盘价
# 											3.根据分钟分组，然后使用count函数统计对应的股票数量
# 3.返回字段：time、count
#实现
select (cur_price - pre_close_price) / pre_close_price as ud, DATE_FORMAT(cur_time,'%Y%m%d%H%i') as time
from stock_rt_info
where cur_time between '2022-01-06 09:30:00' and '2022-01-06 14:25:00'
having ud > 0.1;

select tmp.time,count(*) as count
from (select (cur_price - pre_close_price) / pre_close_price as ud, DATE_FORMAT(cur_time,'%Y%m%d%H%i') as time
      from stock_rt_info
      where cur_time between '2022-01-06 09:30:00' and '2022-01-06 14:25:00'
      having ud >= 0.1) as tmp
group by tmp.time;

# 统计跌停
select tmp.time,count(*) as count
from (select (cur_price - pre_close_price) / pre_close_price as ud, DATE_FORMAT(cur_time,'%Y%m%d%H%i') as time
      from stock_rt_info
      where cur_time between '2022-01-06 09:30:00' and '2022-01-06 14:25:00'
      having ud <= -0.1) as tmp
group by tmp.time;


# 业务需求：统计T日（最新股票交易日）国内大盘每分钟的成交量数据
# 1.相关表：stock_market_index_info
# 2.条件：1.T日代表日期范围，从开盘时间到最新交易时间点
# 				2.A股大盘包含上证和深证 market_code int ()
# 				3.group by 根据时间分组， sum(trade_amt)成交量之和
# 3.time精确到分钟的时间，count：成交量之和
SELECT
    date_format(cur_time,'%Y%m%d%H%i') AS time,
    sum( trade_amount ) AS count
FROM
    stock_market_index_info
WHERE
    cur_time BETWEEN '2022-01-03 09:30:00'
        AND '2022-01-03 14:40:00'
  AND market_code IN ( 'sh000001', 'sz399001' )
GROUP BY
    cur_time
ORDER BY
    cur_time ASC;


# 业务需求：统计在最新交易时间点下A股个股在各个涨幅区间的数量"<-7%" 、 "-7~-5%"、 "-5~-3%" 、 "-3~0%" 、"0~3%" 、 "3~5%" 、 "5~7%" 、 ">7%"
# 1.相关的表 stock_rt_info
# 2.确定条件：  1.获取最新股票交易时间点（精确到分钟）
# 	 						2.涨幅区间与涨幅有关，而涨幅与当前价格前收盘价有关，所以要先获取涨幅的流水
# 3.数据：涨幅区间、涨幅区间对应的股票数量

# sql逻辑
# 1.获取当前最新交易时间点下股票的涨幅流水 0.01
select (cur_price - pre_close_price) / pre_close_price as rate from stock_rt_info where cur_time='2022-01-06 09:55:00';
# 2.判断每个涨幅所处的区间，然后转换为区间标题 “-2~0%”
SELECT
    CASE
        WHEN tmp.rate > 0.07 THEN  '>7%'
        WHEN tmp.rate > 0.05  AND tmp.rate <= 0.07 THEN '5~7%'
        WHEN tmp.rate > 0.03  AND tmp.rate <= 0.05 THEN '3~5%'
        WHEN tmp.rate > 0     AND tmp.rate <= 0.03 THEN '0~3%'
        WHEN tmp.rate > -0.03 AND tmp.rate <= 0 THEN '-3~0%'
        WHEN tmp.rate > -0.05 AND tmp.rate <= -0.03 THEN '-5~-3%'
        WHEN tmp.rate > -0.07 AND tmp.rate <= -0.05 THEN '-7~-5%'
        ELSE '<-7%'
        END 'title'
FROM
    (select (cur_price - pre_close_price) / pre_close_price as rate from stock_rt_info where cur_time='2022-01-06 09:55:00') AS tmp;
# 3.根据区间分组，统计每个区间重复的行记录数 （股票的个数）
select title,count(*) from (SELECT
    CASE
        WHEN tmp.rate > 0.07 THEN  '>7%'
        WHEN tmp.rate > 0.05  AND tmp.rate <= 0.07 THEN '5~7%'
        WHEN tmp.rate > 0.03  AND tmp.rate <= 0.05 THEN '3~5%'
        WHEN tmp.rate > 0     AND tmp.rate <= 0.03 THEN '0~3%'
        WHEN tmp.rate > -0.03 AND tmp.rate <= 0 THEN '-3~0%'
        WHEN tmp.rate > -0.05 AND tmp.rate <= -0.03 THEN '-5~-3%'
        WHEN tmp.rate > -0.07 AND tmp.rate <= -0.05 THEN '-7~-5%'
        ELSE '<-7%'
        END 'title'
FROM
    (select (cur_price - pre_close_price) / pre_close_price as rate from stock_rt_info where cur_time='2022-01-06 09:55:00') AS tmp) as tmp2 group by tmp2.title;