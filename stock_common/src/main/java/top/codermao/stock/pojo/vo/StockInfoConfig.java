package top.codermao.stock.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author msk
 * @date 2024/4/17 20:22
 * @Description 定义股票相关值对象的封装
 */

@ApiModel(description = "定义股票相关值对象的封装")
@Data
@Component
@ConfigurationProperties(prefix = "stock")
public class StockInfoConfig {
    /**
     * 封装国内A股大盘编码集合
     */
    @ApiModelProperty("封装国内A股大盘编码集合")
    private List<String> inner;

    /**
     * 外盘编码集合
     */
    @ApiModelProperty("外盘编码集合")
    private List<String> outer;

    /**
     * 股票涨幅区间标题集合
     */
    @ApiModelProperty("股票涨幅区间标题集合")
    private List<String> upDownRange;

    /**
     * 大盘、外盘、个股的公共url
     */
    @ApiModelProperty("大盘、外盘、个股的公共url")
    private String marketUrl;

    /**
     * 板块采集url
     */
    @ApiModelProperty("板块采集url")
    private String blockUrl;
}
