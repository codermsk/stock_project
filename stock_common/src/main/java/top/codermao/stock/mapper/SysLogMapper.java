package top.codermao.stock.mapper;

import top.codermao.stock.pojo.entity.SysLog;

/**
* @author 92609
* @description 针对表【sys_log(系统日志)】的数据库操作Mapper
* @createDate 2024-04-09 21:37:47
* @Entity top.codermao.stock.pojo.entity.SysLog
*/
public interface SysLogMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysLog record);

    int insertSelective(SysLog record);

    SysLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysLog record);

    int updateByPrimaryKey(SysLog record);

}
