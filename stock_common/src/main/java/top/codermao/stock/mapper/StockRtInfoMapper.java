package top.codermao.stock.mapper;

import org.apache.ibatis.annotations.Param;
import top.codermao.stock.pojo.domain.Stock4EvrDayDomain;
import top.codermao.stock.pojo.domain.Stock4MinuteDomain;
import top.codermao.stock.pojo.domain.StockUpdownDomain;
import top.codermao.stock.pojo.entity.StockRtInfo;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author 92609
* @description 针对表【stock_rt_info(个股详情信息表)】的数据库操作Mapper
* @createDate 2024-04-09 21:37:47
* @Entity top.codermao.stock.pojo.entity.StockRtInfo
*/
public interface StockRtInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockRtInfo record);

    int insertSelective(StockRtInfo record);

    StockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockRtInfo record);

    int updateByPrimaryKey(StockRtInfo record);

    /**
     * 查询指定时间点下的交易数据
     * @param curDate
     * @return
     */
    List<StockUpdownDomain> getStockInfoByTime(@Param("timePoint") Date curDate);

    /**
     * 查询部分最新的股票交易数据
     * @param curTime
     * @return
     */
    List<StockUpdownDomain> getStockInfoSegment(@Param("timePoint") Date curTime);

    /**
     * 统计指定范围内股票涨停或跌停的数量流水
     * @param startDate 开盘时间
     * @param endDate 截止时间
     * @param flag 约定：1.表示统计涨停。0.表示跌停
     * @return
     */
    List<Map> getStockUpdownCount(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("flag") int flag);

    /**
     * 统计指定时间点下股票在各个涨跌区间的数量
     * @param curDate
     * @return
     */
    List<Map> getIncreaseRangeInfoByDate(@Param("dateTime") Date curDate);

    /**
     * 根据股票编码查询指定时间范围内的分时数据
     * @param openDate 开盘时间
     * @param endDate 截止时间，一般与开盘时间同一天
     * @param stockCode 股票编码
     * @return
     */
    List<Stock4MinuteDomain> getStock4MinuteInfo(@Param("openDate") Date openDate, @Param("endDate") Date endDate, @Param("stockCode") String stockCode);

    /**
     *  根据股票编码查询指定时间范围内的日K线数据
     * @param startDate 开盘时间
     * @param endDate 截止时间
     * @param stockCode 股票编码
     * @return
     */
    List<Stock4EvrDayDomain> getStock4DkLine(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("stockCode") String stockCode);
}
