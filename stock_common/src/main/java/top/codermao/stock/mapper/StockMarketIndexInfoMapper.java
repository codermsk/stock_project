package top.codermao.stock.mapper;

import org.apache.ibatis.annotations.Param;
import top.codermao.stock.pojo.domain.InnerMarketDomain;
import top.codermao.stock.pojo.entity.StockMarketIndexInfo;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author 92609
* @description 针对表【stock_market_index_info(国内大盘数据详情表)】的数据库操作Mapper
* @createDate 2024-04-09 21:37:47
* @Entity top.codermao.stock.pojo.entity.StockMarketIndexInfo
*/
public interface StockMarketIndexInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockMarketIndexInfo record);

    int insertSelective(StockMarketIndexInfo record);

    StockMarketIndexInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockMarketIndexInfo record);

    int updateByPrimaryKey(StockMarketIndexInfo record);

    /**
     * 根据指定时间点查询指定大盘集合的数据
     * @param curDate 指定时间点
     * @param marketCodes 大盘编码集合
     * @return
     */
    List<InnerMarketDomain> getInnerMarketInfo(@Param("curDate") Date curDate, @Param("marketCodes") List<String> marketCodes);

    /**
     * 统计指定日期范围内指定大盘每分钟的成交量流水信息
     * @param openDate 起始时间，一般指开盘时间
     * @param endDate 截止时间按，一般与openDate同一天
     * @param marketCodes 大盘编码集合
     * @return
     */
    List<Map> getSumAmtInfo(@Param("openDate") Date openDate, @Param("endDate") Date endDate, @Param("marketCodes") List<String> marketCodes);

    /**
     * 批量插入大盘数据
     * @param entities 大盘实体对象集合
     * @return
     */
    int insertBath(@Param("infos") List<StockMarketIndexInfo> entities);
}
