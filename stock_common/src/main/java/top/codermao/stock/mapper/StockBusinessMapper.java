package top.codermao.stock.mapper;

import top.codermao.stock.pojo.entity.StockBusiness;

/**
* @author 92609
* @description 针对表【stock_business(主营业务表)】的数据库操作Mapper
* @createDate 2024-04-09 21:37:47
* @Entity top.codermao.stock.pojo.entity.StockBusiness
*/
public interface StockBusinessMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockBusiness record);

    int insertSelective(StockBusiness record);

    StockBusiness selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockBusiness record);

    int updateByPrimaryKey(StockBusiness record);

}
