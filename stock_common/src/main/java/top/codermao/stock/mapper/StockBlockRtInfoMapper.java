package top.codermao.stock.mapper;

import org.apache.ibatis.annotations.Param;
import top.codermao.stock.pojo.domain.StockBlockDomain;
import top.codermao.stock.pojo.entity.StockBlockRtInfo;

import java.util.Date;
import java.util.List;

/**
* @author 92609
* @description 针对表【stock_block_rt_info(股票板块详情信息表)】的数据库操作Mapper
* @createDate 2024-04-09 21:37:47
* @Entity top.codermao.stock.pojo.entity.StockBlockRtInfo
*/
public interface StockBlockRtInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockBlockRtInfo record);

    int insertSelective(StockBlockRtInfo record);

    StockBlockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockBlockRtInfo record);

    int updateByPrimaryKey(StockBlockRtInfo record);

    List<StockBlockDomain> sectorAllLimit(@Param("timePoint") Date curDate);


}
